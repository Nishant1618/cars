const data = require("./array.js")
const problem1 = require("./test1.js")

// console.log("data",data);

let carid = 33 ;
let result = problem1(data,carid);
if (result == []){
    console.log("Invalid Input")
} else{ 
// console.log("result",result);
console.log(`Car ${carid} is a ${result.car_year} year ${result.car_make} model ${result.car_model}`);
}